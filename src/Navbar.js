import React from 'react';
import { Link} from 'react-router-dom';

function Navbar(props) {
  // var {path,url} = useRouteMatch();
  return (    
    <div>
      <div className="navbar navbar-expand-lg navbar-light bg-dark">
        <div>
          <h2 className="text-white"><b>
            M2M
          </b></h2>
        </div>
        {
          (props.content === "/about") &&                   
          <div class="dropdown ml-4">
            <a href="" class="btn bg-dark" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <h4 className="text-white">Teams</h4>
            </a>
            <div class="dropdown-menu bg-secondary" aria-labelledby="dropdownMenuButton">
              <Link className="dropdown-item text-dark" to="/development">Development</Link>
              <Link className="dropdown-item text-dark" to="/administration">Administration</Link>
              <Link className="dropdown-item text-dark" to="/marketing">Marketing</Link>    
            </div>
          </div>        
        }  
        {
          (props.content !== "/about") 
            ?
          <div className="ml-auto">
            <h4 className="text-white">Login</h4>
          </div>
           :
          <div className="ml-auto">
            <h4 className="text-white">Hello User</h4>
          </div>
        }   
      </div>
    </div>
  );
}

export default Navbar;
