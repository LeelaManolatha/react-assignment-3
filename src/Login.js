import React from 'react';
import {connect} from "react-redux";
import {setLogin} from "./redux/Login/actions"



function Login({login}) {

  const user = () => {
    var name = document.getElementById("username").value;
    var password = document.getElementById("password").value;
    if(name==="admin" && password==="password"){
      login(true)
    }
    else{
      //Login(false)
      alert("Enter correct credentials!")
    }
  }
  
  return (    
    <div>        
      <div className="container-fluid mt-5">    
        <div className="row align-items-center">
          <div className="col-6 mx-auto">
            <div className="jumbotron ">
               <div className="row p-2">
                 <div className="col-md-6 d-flex justify-content-center">
                  <label>Username</label>                  
                 </div>
                 <div className="col-md-6 d-flex justify-content-center">
                  <input id="username"></input>                  
                 </div>                 
               </div>
               <div className="row p-2">
                 <div className="col-md-6 d-flex justify-content-center">
                  <label>Password</label>                  
                 </div>
                 <div className="col-md-6 d-flex justify-content-center">
                  <input type="password" id="password"></input>                  
                 </div>                 
               </div>
               <div className="row">
                 <div className="col d-flex justify-content-center">
                  <button className="btn btn-primary" onClick={user}>
                    Login
                  </button>                  
                </div>                                  
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>    
  );
}

const dispatchToProps = (dispatch) =>{
  return{
    login:(data) => dispatch(setLogin(data))
  }
}

export default connect(null, dispatchToProps)(Login)

