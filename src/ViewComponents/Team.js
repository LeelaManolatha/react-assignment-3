import React from 'react';
import {   
  Link,
  useRouteMatch  
} from "react-router-dom";

function Team(props) {

  const {path,url} = useRouteMatch();

  var position = {};
  var teamData = props.teamData;
  var data = props.data;

  return (
    <div className="container">
      <div className="col-sm-12 col-md-8 mx-auto mt-5">        
        <div className="row-12 ">
          <h3 className="row-8 text-center">Welcome to {data && data.charAt(0).toUpperCase() + data.slice(1)}</h3>
          <h4 className="row-8 text-center text-secondary mt-3">Here is Team Analysis</h4>
          <div className="col-md-12 col-lg-8 mx-auto mt-5 border border-dark rounded bg-light">
            {
              teamData &&
              <div className="row mt-3">
              <div className="col-6">
                <h5>Total Members</h5>
              </div>
              <div className="col-6">
                <h5 className="text-center">{teamData.length}</h5>
              </div>
              </div>
            }
            {
              teamData &&
              teamData.map((member,index)=>{
                  //var
                  position[member['position']] = position[member['position']] ?(++position[member['position']]):1;
                   
               }) && 
               
               Object.keys(position).map((key,index)=>{
                 return(
                  <div className="row mt-3">
                  <div className="col-6">
                    <h5>{key}</h5>
                  </div>
                  <div className="col-6">
                    <h5 className="text-center">{position[key]}</h5>
                  </div>
                </div>
                 )
               })              
            }
            <div className="row mt-3">
              <p className="col text-center"><Link to={`${path}/members`}>View full members list</Link></p>
            </div>
          </div>
        </div>
      </div>      
    </div>
  );
}

export default Team;
