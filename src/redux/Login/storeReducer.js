import {SET_LOGIN} from "./actionType"

const initialState = {
    log :false,
};

export default function (state = initialState, action) {
    switch(action.type) {
        case SET_LOGIN: 
            return {
                ...state,
                log : action.payload,
            }
        default: return state
    }
}