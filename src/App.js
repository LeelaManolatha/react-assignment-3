import React from "react";
import About from './About';
import Navbar from './Navbar';
import Login from './Login';
import View from './View';
import { 
  BrowserRouter as Router, 
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import {connect} from "react-redux";

function App({log}) {
  
  return (
  <Router>    
    <div>
      <Switch>       
        <Route eaxct path={["/development","/marketing","/add","/administration"]}>
          <Navbar content="/about"/>
          <View />            
        </Route> 

        <Route path="/about">
          {
            log 
            ?
            <div>
              <Navbar content="/about" />
              <About />
            </div>
            :
            <Redirect to="/" />
          }          
        </Route>   

        <Route path="/">
          {
            log 
            ?            
              <Redirect to="/about" />            
            :
            <div>
              <Navbar content="/" />
              <Login />
            </div>
          }
        </Route>        

      </Switch>
    </div>  
  </Router>  
  );
}

const StatetoProp = (state) =>{
  return{
    log: state.storeReducer.log,
  }
}

export default connect(StatetoProp,null)(App);