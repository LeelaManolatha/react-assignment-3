import React from "react";
import About from './About';
import Navbar from './Navbar';
import Login from './Login';
import View from './View';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useRouteMatch  
} from "react-router-dom";

export default function Next() {

  const {path,url} = useRouteMatch();
  
  return (    
    <div>
      <Switch>
        
      <Route exact path={["/development",]}>          
          <About />
        </Route>   

        <Route exact path="/about">          
          <About />
        </Route>        

      </Switch>
    </div>    
  );
}
