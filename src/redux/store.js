import {createStore, applyMiddleware} from "redux"
import reducer from "./indexReducer"


export default createStore(
    reducer
);