import {SET_LOGIN} from "./actionType"

export const setLogin = (data) =>{
    return{
        type: SET_LOGIN,
        payload: data,
    }
}