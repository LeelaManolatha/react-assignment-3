import React from 'react';
import './App.css';
import {Link} from 'react-router-dom';

function About(props) {

   
   return (
      <div>   
         <div className="container ">
            <div className="col-md-10 col-lg-6 mx-auto rounded bg-light">       
               <div className="row-md-12 mt-5">
                  <h3 className="row-8 text-center d-flex m-auto justify-content-center">Welcome To M2M</h3>          
               </div>
               <div className="row mt-2">
                  <h3 className="row-8 text-center d-flex m-auto align-self-center justify-content-center">Click on a tile to explore more</h3>          
               </div>
               <div className="row mt-3 p-1 ">
                  <div className="col-6 ">
                     <div className="col-12 col-sm-10 col-xl-8 dev d-flex justify-content-center m-auto abc">
                        <Link to="/development" className="text-dark my-auto h5"><b>Development</b></Link>
                     </div>
                  </div>
                  <div className="col-6 ">
                     <div className="col-12 col-sm-10 col-xl-8 mar d-flex m-auto justify-content-center abc">
                        <Link to="/marketing" className="text-dark my-auto h5"><b>Marketing</b></Link> 
                     </div>
                  </div>  
               </div>
               <div className="row mt-5 p-1">
                  <div className="col-6">
                     <div className="col-12 col-sm-10 col-xl-8 adm d-flex justify-content-center m-auto abc">
                        <Link to="/administration" className="text-dark my-auto h5"><b>Administration</b></Link>
                     </div>
                  </div>
                  <div className="col-6">
                     <div className="col-12 col-sm-10 col-xl-8  add d-flex m-auto justify-content-center abc">
                        <Link to="/add" className="text-dark my-auto h5"><b>Add</b></Link>
                     </div>
                  </div>  
               </div>
            </div>
         </div>   
      </div>
  );
}

export default About;