import React,{useState, useEffect} from 'react';
import {  
  Switch,
  Route,  
  useRouteMatch  
} from "react-router-dom";
import Table from './ViewComponents/Table';
import Team from './ViewComponents/Team';


function View(props) {

  const {path,url} = useRouteMatch(); 

  const [data,setData] = useState(null);  
  const [teamData,setteamData] = useState(null);

  useEffect(()=>{
    setData(path.slice(1));  
  })

  useEffect(()=>{
    setData(path.slice(1));
    collectData();
  },[data])

  async function collectData(){
    try {
      const response = await fetch(`https://yayjk.dev/api/members/team/${data}`);
      const json = await response.json();
      setteamData(json['teamMembers']);               
    } catch (error) {
      console.log(error);
    }        
  } 

    
  return (
    <div>
      <Switch>
        <Route exact path={`${path}/members`}>
          <Table data={teamData}/>
        </Route>
        <Route exact path={`${path}`}>
          <Team teamData={teamData} data={data}/>
        </Route>
      </Switch>  
    </div>
  );
}

export default View;
