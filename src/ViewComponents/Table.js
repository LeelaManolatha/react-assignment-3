import React,{useState, useEffect}  from 'react';
import '../App.css';
function Table(props) {

  const [click,setClick] = useState(true)
    const [text,setText] = useState("View More")
   const [toggle,setToggle] = useState(null);
   const [teamData,setteamData] = useState(null);
  useEffect(()=>{
    setteamData(props.data);    
  })

  useEffect(()=>{
    setteamData(props.data);    
    setClick(true);
  },[teamData])

  useEffect(()=>{
        if(click) setText("View More")
        else setText("View Less")
  },[click])


  var month = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
  

  return (
    

      <div>   
        <div className="container">
            <div className="mx-auto mt-5 table-responsive-sm col-md-8 ">
              <table className="table table-hover table-dark">
              <thead>
                  <tr>
                    <th scope="col">S.no</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Phone No</th>
                  </tr>
                </thead>  
                <tbody>
        {
          teamData && 
          teamData.map((member,index)=>{
            return (
              <tr>
                    <th className="table-warning text-dark" scope="row">{index+1}</th>
                    <td className="table-success text-dark">{member['name'].charAt(0).toUpperCase()+member['name'].slice(1)}</td>
                    <td className="table-primary text-dark">{member['email']}</td>
                    <td className="table-danger text-dark">{member['phone']}</td>
                    <td className="table-secondary text-dark"><a className="text-dark" data-toggle="collapse" href="#collapseExample1" aria-expanded="false" aria-controls="collapseExample1" onClick={()=>{setToggle(index+1);setClick(!click)}}>{text}</a></td>
                    
                </tr>
            )

          })
        }                   
                
                  
                 
                  
                </tbody>
              </table>
            </div>        
          </div>    
          <div className="collapse multi-collapse" id="collapseExample1">
          <div className="col-sm-8 col-md-8 col-lg-6 card card-body mx-auto">
            {teamData && toggle && 
            
              <div className="row ">
                <div className="col-6">
                  <h4 className="text-center text-dark font-weight-bold">Position</h4>
                  </div>
                  <div className="col-6 ">
                  <h4 className="text-center text-dark font-italic">{teamData[toggle-1].position}</h4>
                  </div>
                  <div className="col-6">
                    <h4 className="text-center text-dark font-weight-bold">Date of Joining</h4>
                  </div>
                  <div className="col-6">
                  
                    <h4 className="text-center text-dark font-italic">{month[new Date(teamData[toggle-1].dateOfJoining).getMonth()]+" "+new Date(teamData[toggle-1].dateOfJoining).getDate()+","+new Date(teamData[toggle-1].dateOfJoining).getFullYear()}</h4>
                  </div>
                </div>
            }
          </div>
          </div>
      
      </div>
  );
}

export default Table;